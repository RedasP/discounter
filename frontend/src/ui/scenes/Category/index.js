import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Navbar from '../../components/Navbar';
import Result from '../../components/Result';
import { connect } from 'react-redux';
import { fetchItems, receiveItems, clearItems } from 'reducers/item';
import { toggleShop, changeSortOrder } from 'reducers/filter';
import namePairs from '../../services/namePairs';

class Category extends Component {
    constructor(props) {
        super(props);

        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    componentDidMount() {
        if (!this.props.items.isLoading) {
            this.props.clearItems();
            this.props.fetchItems(namePairs[this.props.params.category].query).then(() => {
                this.props.receiveItems();
            })
        }
    }

    onBlur() {
        if (this.state.value == '')
            deactivateSearchbox(document.querySelector('.searchbox'));
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        browserHistory.push({
            pathname: '/search',
            search: '?query=' + this.state.value
        });
    }

    render() {
        return <div>
            <Navbar value={this.state.value}
                onBlur={() => this.onBlur()}
                handleChange={(e) => this.handleChange(e)}
                handleSubmit={(e) => this.handleSubmit(e)} />
            <div className="container" style={{ marginTop: "20px" }}>
                <div className="row">
                    <div className="col-xs-12">
                        <h1 style={{ color: "black", fontFamily: "Cabin", fontSize: "20px" }}>
                            Produktas: {namePairs[this.props.params.category].name}
                        </h1>
                    </div>
                </div>
            </div>
            <Result items={this.props.items.items}
                filter={this.props.filter}
                displayItems={this.props.displayItems}
                toggleShop={this.props.toggleShop}
                changeSortOrder={this.props.changeSortOrder} />
        </div>
    }
}

export default connect(
    state => (
        {
            items: state.item,
            displayItems: state.item.displayItems,
            filter: state.filter
        }
    ),
    {
        fetchItems,
        receiveItems,
        clearItems,
        toggleShop,
        changeSortOrder
    }
)(Category);