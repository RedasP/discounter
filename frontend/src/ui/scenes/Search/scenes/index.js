import React, { Component } from 'react';
import SearchBar from './SearchBar';
import Result from '../../../components/Result';

export default class SearchComponent extends Component {
  render() {
    return (
      <div>
        <SearchBar fetchItems={this.props.fetchItems}
          receiveItems={this.props.receiveItems}
          clearItems={this.props.clearItems}
          query={this.props.location}
          displayItems={this.props.displayItems} />
        <Result items={this.props.items}
          filter={this.props.filter}
          displayItems={this.props.displayItems}
          toggleShop={this.props.toggleShop}
          changeSortOrder={this.props.changeSortOrder}
        />
      </div>
    )
  }
}
