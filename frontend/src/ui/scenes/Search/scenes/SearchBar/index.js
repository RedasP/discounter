import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Navbar from '../../../../components/Navbar';

function activateSearchbox(el) {
    el.classList.add('searchbox--active')
    document.getElementById("searchbox").placeholder = "Įveskite produkto pavadinimą, pvz., bananai";
}
function deactivateSearchbox(el) {
    el.classList.remove('searchbox--active')
    document.getElementById("searchbox").placeholder = "";
}
function onFocus() {
    activateSearchbox(document.querySelector('.searchbox'));
}

export default class SearchForm extends Component {

    constructor(props) {
        super(props);

        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        let query = this.props.query.query.query;
        this.props.clearItems();
        this.props.fetchItems(query).then(() => {
            this.props.receiveItems();
        })
        this.setState({ value: query });
        
        activateSearchbox(document.querySelector('.searchbox'));
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault()
        if (this.props.displayItems) {
            browserHistory.replace({
                pathname: '/search',
                search: '?query=' + this.state.value
            });
            this.props.clearItems();
            this.props.fetchItems(this.state.value).then(() => {
                this.props.receiveItems();
            })
        }
    }

    onBlur() {
        if (this.state.value == '')
            deactivateSearchbox(document.querySelector('.searchbox'));
    }

    render() {
        return (
            <Navbar value={this.state.value} onBlur={() => this.onBlur()} handleChange={(e) => this.handleChange(e)} handleSubmit={(e) => this.handleSubmit(e)}/>
        )
    }
}
