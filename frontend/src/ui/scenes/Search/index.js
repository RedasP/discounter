import SearchComponent from './scenes';
import { connect } from 'react-redux';
import { fetchMaximaItems, fetchLidlItems, fetchKubasItems, fetchItems, receiveItems, clearItems } from 'reducers/item';
import { toggleShop, changeSortOrder } from 'reducers/filter';

export default connect(
  state => (
    {
      items: state.item.items,
      displayItems: state.item.displayItems,
      filter: state.filter
    }
  ),
  {
    fetchMaximaItems,
    fetchLidlItems,
    fetchKubasItems,
    fetchItems,
    receiveItems,
    clearItems,
    toggleShop,
    changeSortOrder
  }
)(SearchComponent);
