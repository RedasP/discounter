import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Navbar from '../../components/Navbar';

function activateSearchbox(el) {
    el.classList.add('searchbox--active')
    document.getElementById("searchbox").placeholder = "Įveskite produkto pavadinimą, pvz., bananai";
}
function deactivateSearchbox(el) {
    el.classList.remove('searchbox--active')
    document.getElementById("searchbox").placeholder = "";
}
function onFocus() {
    activateSearchbox(document.querySelector('.searchbox'));
}

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    onBlur() {
        if (this.state.value == '')
            deactivateSearchbox(document.querySelector('.searchbox'));
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        browserHistory.push({
            pathname: '/search',
            search: '?query=' + this.state.value
        });
    }

    render() {
        return (
            <div>
                <Navbar value={this.state.value}
                    onBlur={() => this.onBlur()}
                    handleChange={(e) => this.handleChange(e)}
                    handleSubmit={(e) => this.handleSubmit(e)} />
                <div className="background">
                    <img className="logo" src="https://i.imgur.com/NPG1cyX.png" />
                    <h1 className="logotext" style={{ textAlign: "center" }}>Parduotuvių akcijos ir kainos</h1>
                </div>
            </div>
        )
    }
}

