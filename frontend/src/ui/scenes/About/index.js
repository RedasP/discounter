import React, { Component } from 'react';
import Navbar from '../../components/Navbar';
import { browserHistory } from 'react-router';

function deactivateSearchbox(el) {
    el.classList.remove('searchbox--active')
    document.getElementById("searchbox").placeholder = "";
}

class About extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    onBlur() {
        if (this.state.value == '')
            deactivateSearchbox(document.querySelector('.searchbox'));
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        browserHistory.push({
            pathname: '/search',
            search: '?query=' + this.state.value
        });
    }

    render() {
        return <div>
            <Navbar value={this.state.value}
                onBlur={() => this.onBlur()}
                handleChange={(e) => this.handleChange(e)}
                handleSubmit={(e) => this.handleSubmit(e)} />
            <div className="container" style={{ marginTop: "20px" }}>
                <div className="row">
                    <div className="col-xs-12">
                        <h1 style={{ color: "black", fontFamily: "Cabin" }}>
                            Apie projektą
                        </h1>
                        <p style={{ color: "black", fontFamily: "Cabin", marginBottom: "10px" }}>Shopper yra paieškos sistema, skirta ieškoti ir palyginti prekybos tinklų produktų kainas ir akcijas.</p>
                        <p style={{ color: "black", fontFamily: "Cabin", marginBottom: "10px" }}>Sistema Jums gali padėti atrasti norimą produktą už geriausią kainą.</p>
                        <p style={{ color: "black", fontFamily: "Cabin", marginBottom: "10px" }}>Šiuo metu įmanoma rasti Maxima, IKI, Rimi, Lidl, Norfa, Šilas ir Kubas produktus.</p>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default About;
