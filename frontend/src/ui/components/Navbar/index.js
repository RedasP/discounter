import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { fetchItems, receiveItems, clearItems } from 'reducers/item';
import { connect } from 'react-redux';
import CategoryDropdown from './CategoryDropdown';
import namePairs from '../../services/namePairs';

function activateSearchbox(el) {
    el.classList.add('searchbox--active')
    document.getElementById("searchbox").placeholder = "Įveskite produkto pavadinimą, pvz., bananai";
}
function deactivateSearchbox(el) {
    el.classList.remove('searchbox--active')
    document.getElementById("searchbox").placeholder = "";
}
function onFocus() {
    activateSearchbox(document.querySelector('.searchbox'));
}

class Navbar extends Component {
    handleClick = (e) => {
        e.preventDefault();
        if (e.target.id == "home")
            browserHistory.push("/");
        else if (e.target.id == "about")
            browserHistory.push("/about");
    }

    handleCategory = (e) => {
        e.preventDefault();
        if (e.target.id != "") {
            browserHistory.push("/" + e.target.id);
            this.props.clearItems();
            this.props.fetchItems(namePairs[e.target.id].query).then(() => {
                this.props.receiveItems();
            })
        }
    }

    render() {
        return <nav className="container-fluid">
            <div className="row" style={{ display: "flex", alignItems: "center", backgroundColor: "#8C6335" }}>
                <div className="col-md-3 col-xs-12" >
                    <a href="/" onClick={this.handleClick} style={{ color: "white", fontFamily: "Lobster", fontSize: "30px" }} id="home">Shopper</a>
                </div>
                <div className="col-md-4 col-xs-12">
                    <form onSubmit={this.props.handleSubmit}>
                        <div className="searchbox">
                            <input type="text"
                                onChange={this.props.handleChange}
                                onFocus={() => onFocus()}
                                onBlur={this.props.onBlur}
                                value={this.props.value}
                                id="searchbox" />
                            <button type="submit">
                                <i className="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div className="col-md-2 col-xs-12">
                    <CategoryDropdown handleCategory={(e) => this.handleCategory(e)} />
                </div>
                <div className="col-md-2 col-xs-12">
                    <a href="/about" onClick={this.handleClick} style={{ color: "white", fontFamily: "Cabin", fontSize: "20px" }} id="about">Apie projektą</a>
                </div>
            </div>
        </nav>
    }
}

export default connect(
    null,
    {
        fetchItems,
        receiveItems,
        clearItems
    }
)(Navbar);