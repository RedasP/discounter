import React, { Component } from 'react';

class CategoryDropdown extends Component {
    render() {
        return <div className="dropdown">
            <span className="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                style={{ color: "white", fontFamily: "Cabin", fontSize: "20px" }}>
                Kategorijos
            </span>
            <ul onClick={this.props.handleCategory} className="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                <li className="dropdown-submenu">
                    <a className="dropdown-item" href="">Mėsa</a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item"><a href="/vistiena" id="vistiena">Vištiena</a></li>
                        <li className="dropdown-item"><a href="/kiauliena" id="kiauliena">Kiauliena</a></li>
                        <li className="dropdown-item"><a href="/jautiena" id="jautiena">Jautiena</a></li>
                        <li className="dropdown-item"><a href="/aviena" id="aviena">Aviena</a></li>
                    </ul>
                </li>
                <li className="dropdown-submenu">
                    <a className="dropdown-item" href="">Žuvys</a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item"><a href="/silke" id="silke">Silkė</a></li>
                        <li className="dropdown-item"><a href="/lasisa" id="lasisa">Lašiša</a></li>
                        <li className="dropdown-item"><a href="/tunas" id="tunas">Tunas</a></li>
                        <li className="dropdown-item"><a href="/krabai" id="krabai">Krabų gaminiai</a></li>
                    </ul>
                </li>
                <li className="dropdown-submenu">
                    <a className="dropdown-item" href="">Daržovės</a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item"><a href="/agurkai" id="agurkai">Agurkai</a></li>
                        <li className="dropdown-item"><a href="/pomidorai" id="pomidorai">Pomidorai</a></li>
                        <li className="dropdown-item"><a href="/paprikos" id="paprikos">Paprikos</a></li>
                        <li className="dropdown-item"><a href="/bulves" id="bulves">Bulvės</a></li>
                        <li className="dropdown-item"><a href="/morkos" id="morkos">Morkos</a></li>
                        <li className="dropdown-item"><a href="/kopustai" id="kopustai">Kopūstai</a></li>
                        <li className="dropdown-item"><a href="/svogunai" id="svogunai">Svogūnai</a></li>
                        <li className="dropdown-item"><a href="/cesnakai" id="cesnakai">Česnakai</a></li>
                        <li className="dropdown-item"><a href="/salotos" id="salotos">Salotos</a></li>
                    </ul>
                </li>
                <li className="dropdown-submenu">
                    <a className="dropdown-item" href="">Vaisiai</a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item"><a href="/bananai" id="bananai">Bananai</a></li>
                        <li className="dropdown-item"><a href="/obuoliai" id="obuoliai">Obuoliai</a></li>
                        <li className="dropdown-item"><a href="/kriauses" id="kriauses">Kriaušės</a></li>
                        <li className="dropdown-item"><a href="/apelsinai" id="apelsinai">Apelsinai</a></li>
                        <li className="dropdown-item"><a href="/mandarinai" id="mandarinai">Mandarinai</a></li>
                        <li className="dropdown-item"><a href="/vynuoges" id="vynuoges">Vynuogės</a></li>
                    </ul>
                </li>
                <li className="dropdown-submenu">
                    <a className="dropdown-item" href="">Pieno produktai</a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item"><a href="/pienas" id="pienas">Pienas</a></li>
                        <li className="dropdown-item"><a href="/suris" id="suris">Sūris</a></li>
                        <li className="dropdown-item"><a href="/grietine" id="grietine">Grietinė</a></li>
                        <li className="dropdown-item"><a href="/varske" id="varske">Varškė</a></li>
                        <li className="dropdown-item"><a href="/sureliai" id="sureliai">Sūreliai</a></li>
                        <li className="dropdown-item"><a href="/sviestas" id="sviestas">Sviestas</a></li>
                        <li className="dropdown-item"><a href="/margarinas" id="margarinas">Margarinas</a></li>
                        <li className="dropdown-item"><a href="/jogurtas" id="jogurtas">Jogurtas</a></li>
                    </ul>
                </li>
                <li className="dropdown-item"><a href="/kiausiniai" id="kiausiniai">Kiaušiniai</a></li>
            </ul>
        </div>

    }
}

export default CategoryDropdown;
