import React, { Component } from 'react';
import Item from '../Item';
import sort from './services/sort';

export default class Shop extends Component {

    constructor(props) {
        super(props);

        this.mapItems = this.mapItems.bind(this);
    }

    mapItems() {
        if (this.props.items.error)
            return <div className="col-xs-12" style={{ backgroundColor: "#fff", marginBottom: "20px" }}>
                Įvyko klaida. Klaidos pranešimas: {this.props.items.error}
            </div>
        if (this.props.items.items.length == 0)
            return <div className="col-xs-12" style={{ backgroundColor: "#fff", marginBottom: "20px" }}>
                Akcijų nerasta
            </div>

        let order = this.props.order;
        let items = Object.assign([], this.props.items.items);

        if (order != "default") {
            sort(items, order);
        }

        return items.map((value, index) => {
            return <Item item={value} key={index} index={index + 1} shopName={this.props.items.name} logoURL={this.props.items.logoURL} />
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-12 shop-name" style={{ "backgroundColor": "#fff" }}>
                        <img className="shop-logo" src={this.props.items.logoURL} alt={this.props.items.name+" akcijos"} />
                    </div>
                </div>
                <ul className="products">
                    <div className="row">
                        {this.mapItems()}
                    </div>
                </ul>
            </div>
        )
    }
}