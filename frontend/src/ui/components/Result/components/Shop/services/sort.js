export default (items, order) => {
    if (order != "default") {
        items.sort((a, b) => {
            if (order == "price-asc") {
                if (a.price > b.price)
                    return 1;
                if (a.price < b.price)
                    return -1;
                return 1;
            } else if (order == "price-desc") {
                if (a.price < b.price)
                    return 1;
                if (a.price > b.price)
                    return -1;
                return 1;
            } else if (order == "weight") {
                if (a.amount > b.amount)
                    return 1;
                if (a.amount < b.amount)
                    return -1;
                return 1;
            }
            return 1;
        });
    }
}
