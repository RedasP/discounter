import React, { Component } from 'react';

export default class ShopFilter extends Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(name) {
        this.props.toggleShop(name);
    }

    mapCheckboxes() {
        return this.props.visibility.map((shop, index) => {
            return <div className="col-xs-2"
                key={index}>
                <label className="form-check-label checkbox-space">
                    <input type="checkbox"
                        defaultChecked={shop.show}
                        name="shop"
                        value={shop.name}
                        onClick={() => this.handleClick(shop.name)} /> {shop.name}
                </label>
            </div>
        })
    }

    render() {
        return (
            <div className="row">
                <div>
                    Rodomi prekybos tinklai:
                </div>
                {this.mapCheckboxes()}
            </div>
        )
    }
}
