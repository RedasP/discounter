import React, { Component } from 'react';

export default class Sort extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.props.changeSortOrder(e.target.value);
    }

    render() {
        return (
            <div>
                <div className="row filter-row">
                    <div className="col-xs-12 form-inline">
                        <label>Rikiuoti pagal:</label>
                        <select className="sort-by form-control" onChange={(e) => this.handleChange(e)} value={this.props.order}>
                            <option value="default">-</option>
                            <option value="price-asc">Kainą: nuo mažiausios iki didžiausios</option>
                            <option value="price-desc">Kainą: nuo didžiausios iki mažiausios</option>
                        </select>
                    </div>
                </div>
            </div>
        )
    }
}