import React, { Component } from 'react';

import ShopFilter from './components/ShopFilter'
import Sort from './components/Sort'

export default class Filter extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="container" style={{ marginTop: "20px" }}>
                    <div className="row">
                        <div className="col-xs-12">
                            Iš viso rezultatų: {this.props.itemCount}
                        </div>
                    </div>
                    <ShopFilter toggleShop={this.props.toggleShop}
                        visibility={this.props.visibility} />
                    <Sort changeSortOrder={this.props.changeSortOrder}
                        order={this.props.order} />
                </div>
            </div>
        )
    }
}
