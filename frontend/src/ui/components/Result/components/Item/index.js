import React, { Component } from 'react';

export default class Item extends Component {

    constructor(props) {
        super(props)
        this.renderItem = this.renderItem.bind(this);
        this.renderPriceOrDiscount = this.renderPriceOrDiscount.bind(this);
    }

    renderPriceOrDiscount() {
        if (this.props.item.price)
            return <p>{this.props.item.price} €</p>
        else
            return <p>{this.props.item.discount}</p>
    }

    renderItem() {
        return (
            <div className="product col-lg-3 col-sm-4" style={{ "backgroundColor": "#fff" }}>
                <div className="image-container">
                    <img src={this.props.item.imageURL} alt={this.props.item.name} />
                </div>
                <div className="details-container">
                    {this.props.item.name}
                </div>
                <img src={this.props.logoURL} style={{ height: "20px", width: "auto", "float": "right" }} />
                {this.renderPriceOrDiscount()}
            </div>
        );
    }

    render() {
        return this.renderItem();
    }
}