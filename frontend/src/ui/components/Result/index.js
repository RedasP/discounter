import React, { Component } from 'react';
import Shop from './components/Shop'
import Filter from './components/Filter'

export default class ResultComponent extends Component {

    constructor(props) {
        super(props);

        this.getItemCount = this.getItemCount.bind(this);
        this.mapShops = this.mapShops.bind(this);
    }

    mapShops() {
        return this.props.items.map((shop, index) => {
            let isVisible = false;
            for (let i = 0; i < this.props.filter.visibility.length; i++)
                if (this.props.filter.visibility[i].name == shop.name && this.props.filter.visibility[i].show) {
                    isVisible = true;
                    break;
                }
            if (isVisible)
                return <Shop items={shop} key={index} order={this.props.filter.order} />
        });
    }

    getItemCount() {
        let itemCount = 0;
        for (let i = 0; i < this.props.items.length; i++)
            for (let x = 0; x < this.props.filter.visibility.length; x++)
                if (this.props.filter.visibility[x].name == this.props.items[i].name && this.props.filter.visibility[x].show)
                    itemCount += this.props.items[i].items.length;
        return itemCount;
    }

    render() {
        if (this.props.displayItems === true) {
            let itemCount = this.getItemCount();
            return (
                <div>
                    <Filter itemCount={itemCount}
                        toggleShop={this.props.toggleShop}
                        visibility={this.props.filter.visibility}
                        order={this.props.filter.order}
                        changeSortOrder={this.props.changeSortOrder} />
                    <div>
                        {this.mapShops()}
                    </div>
                </div>
            )
        } else {
            return <div className="loader"></div>
        }
    }
}
