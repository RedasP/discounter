export default {
    //pienas
    pienas: {
        query: 'pienas',
        name: 'pienas'
    },
    suris: {
        query: 'sūris',
        name: 'sūris'
    },
    grietine: {
        query: 'grietinė',
        name: 'grietinė'
    },
    varske: {
        query: 'varškė',
        name: 'varškė'
    },
    sureliai: {
        query: 'sūrel',
        name: 'sūrelis'
    },
    sviestas: {
        query: 'sviestas',
        name: 'sviestas'
    },
    margarinas: {
        query: 'margarinas',
        name: 'margarinas'
    },
    jogurtas: {
        query: 'jogurt',
        name: 'jogurtas'
    },
    //vaisiai
    bananai: {
        query: 'bananai',
        name: 'bananai'
    },
    obuoliai: {
        query: 'obuoliai',
        name: 'obuoliai'
    },
    kriauses: {
        query: 'kriaušės',
        name: 'kriaušės'
    },
    apelsinai: {
        query: 'apelsinai',
        name: 'apelsinai'
    },
    mandarinai: {
        query: 'mandarinai',
        name: 'mandarinai'
    },
    vynuoges: {
        query: 'vynuogės',
        name: 'vynuogės'
    },
    //daržovės
    agurkai: {
        query: 'agurkai',
        name: 'agurkai'
    },
    pomidorai: {
        query: 'pomidor',
        name: 'pomidorai'
    },
    paprikos: {
        query: 'papriko',
        name: 'paprikos'
    },
    bulves: {
        query: 'bulv',
        name: 'bulvės'
    },
    morkos: {
        query: 'morkos',
        name: 'morkos'
    },
    kopustai: {
        query: 'kopūstai',
        name: 'kopūstai'
    },
    svogunai: {
        query: 'svogūnai',
        name: 'svogūnai'
    },
    cesnakai: {
        query: 'česnakai',
        name: 'česnakai'
    },
    salotos: {
        query: 'salotos',
        name: 'salotos'
    },
    //mėsa
    vistiena: {
        query: 'vištiena',
        name: 'vištiena'
    },
    kiauliena: {
        query: 'kiauliena',
        name: 'kiauliena'
    },
    jautiena: {
        query: 'jautiena',
        name: 'jautiena'
    },
    aviena: {
        query: 'aviena',
        name: 'aviena'
    },
    //žuvys
    silke: {
        query: 'silkė',
        name: 'silkė'
    },
    lasisa: {
        query: 'lašiša',
        name: 'lašiša'
    },
    tunas: {
        query: 'tunas',
        name: 'tunas'
    },
    krabai: {
        query: 'krab',
        name: 'krabai'
    },
    //kiaušiniai
    kiausiniai: {
        query: 'kiaušiniai',
        name: 'kiaušiniai'
    }
}