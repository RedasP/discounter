import React from 'react';
import { Route, Redirect, IndexRoute } from 'react-router';
import App from '../ui/App';
import MainSearchComponent from '../ui/scenes/Home';
import SearchPage from '../ui/scenes/Search';
import Category from '../ui/scenes/Category';
import About from '../ui/scenes/About';

export default (onLogout) => (
  <Route path="/" name="app" component={App}>
    <IndexRoute component={MainSearchComponent}/>
    <Route path="search" component={SearchPage}/>
    <Route path="about" component={About}/>
    <Route path=":category" component={Category}/>
  </Route>
);
