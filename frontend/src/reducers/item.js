const FETCH = 'item/FETCH';
const FETCH_SUCCESS = 'item/FETCH_SUCCESS';
const FETCH_FAIL = 'item/FETCH_FAIL';

const RECEIVE_ITEMS = 'item/RECEIVE_ITEMS';
const CLEAR_ITEMS = 'item/CLEAR_ITEMS';

const defaultState = {
  displayItems: false,
  isLoading: false,
  items: []
}

export default function itemReducer(state = defaultState, action) {
  switch (action.type) {
    case CLEAR_ITEMS:
      return {
        displayItems: false,
        isLoading: false,
        items: []
      }
    case RECEIVE_ITEMS:
      return {
        displayItems: true,
        isLoading: false,
        items: [
          ...state.items
        ]
      };
    case FETCH: 
      return {
        ...state,
        isLoading: true
      }
    case FETCH_FAIL:
      return {
        ...state,
        displayItems: false,
        items: [
          ...state.items,
          {
            name: action.name,
            logoURL: action.imageURL,
            items: [],
            error: action.error.message,
            errorCode: action.error.code
          }
        ]
      }
    case FETCH_SUCCESS: {
      let items = Object.assign([], state.items);
      let item = {
        name: action.result.data.name,
        logoURL: action.result.data.logoURL,
        items: action.result.data.itemDetails.map(
          (element) => ({
            name: element.item.name,
            imageURL: element.imageURL,
            price: element.price,
            discount: element.discount
          })
        )
      };

      action.result.data.itemDetails.length == 0 ? items.push(item) : items.unshift(item);
      return {
        displayItems: false,
        isLoading: true,
        items
      };
    };
    default: {
      return state;
    }

  }
}

export function clearItems() {
  return {
    type: CLEAR_ITEMS
  }
}

export function receiveItems() {
  return {
    type: RECEIVE_ITEMS,
    displayItems: true
  }
}

export function fetchItems(query) {
  return dispatch => Promise.all([
    dispatch(fetchMaximaItems(query)),
    dispatch(fetchLidlItems(query)),
    dispatch(fetchKubasItems(query)),
    dispatch(fetchIkiItems(query)),
    dispatch(fetchRimiItems(query)),
    dispatch(fetchNorfaItems(query)),
    dispatch(fetchSilasItems(query))
  ]);
}

export function fetchMaximaItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Maxima",
    imageURL: "https://upload.wikimedia.org/wikipedia/commons/c/c1/Maxima_logo.svg",
    promise: client => client.get('/api/Maxima/' + query)
  };
}

export function fetchLidlItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Lidl",
    imageURL: "https://seeklogo.com/images/L/Lidl-logo-98D452E228-seeklogo.com.png",
    promise: client => client.get('/api/Lidl/' + query)
  };
}

export function fetchKubasItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Kubas",
    imageURL: "http://www.pckubas.lt/image/catalog/logo2.png",
    promise: client => client.get('/api/Kubas/' + query)
  };
}

export function fetchIkiItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "IKI",
    imageURL: "http://lofrev.net/wp-content/photos/2017/03/Iki_logo_2.png",
    promise: client => client.get('/api/Iki/' + query)
  };
}

export function fetchRimiItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Rimi",
    imageURL: "http://www.rimi.lt/assets/upload/userfiles/images/logos%20and%20images/Rimi%20SM%20logo%20su%20seseliais.jpg",
    promise: client => client.get('/api/Rimi/' + query)
  };
}

export function fetchNorfaItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Norfa",
    imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Norfa_Logo.svg/1280px-Norfa_Logo.svg.png",
    promise: client => client.get('/api/Norfa/' + query)
  };
}

export function fetchSilasItems(query) {
  return {
    types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
    name: "Šilas",
    imageURL: "http://silas.lt/wp-content/uploads/2015/01/logo-be-sukio.png",
    promise: client => client.get('/api/Silas/' + query)
  };
}