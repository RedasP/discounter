const TOGGLE_SHOP = "filter/TOGGLE_SHOP";
const CHANGE_SORT_ORDER = "filter/CHANGE_SORT_ORDER";

const initialState = {
    order: "default",
    visibility: [
        {
            name: "Maxima",
            show: true
        },
        {
            name: "Lidl",
            show: true
        },
        {
            name: "Kubas",
            show: true
        },
        {
            name: "IKI",
            show: true
        },
        {
            name: "Rimi",
            show: true
        },
        {
            name: "Norfa",
            show: true
        },
        {
            name: "Šilas",
            show: true
        }
    ]
}

export default function filterReducer(state = initialState, action) {
    switch (action.type) {
        case TOGGLE_SHOP: {
            return {
                order: state.order,
                visibility: state.visibility.map((shop, index) => {
                    if (shop.name == action.name) {
                        return Object.assign({}, shop, {
                            show: !shop.show
                        })
                    }
                    return shop;
                })
            }
        }
        case CHANGE_SORT_ORDER:
            return {
                order: action.order,
                visibility: state.visibility
            }
        default:
            return state;
    }
}

export function toggleShop(name) {
    return {
        type: TOGGLE_SHOP,
        name
    }
}

export function changeSortOrder(order) {
    return {
        type: CHANGE_SORT_ORDER,
        order
    }
}