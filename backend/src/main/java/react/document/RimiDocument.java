package react.document;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class RimiDocument implements JsoupDocument {
  @Override
  public Document getDocument(String URL, String itemName) {
    Document document = null;

    Connection.Response res = null;
    try {
      res = Jsoup.connect("https://app.rimi.lt")
        .maxBodySize(0)
        .method(Connection.Method.GET)
        .execute();
    } catch (IOException e) {
      e.printStackTrace();
    }

    Map<String, String> mainCookies = res.cookies();
    try {
      int randomUserAgent = ThreadLocalRandom.current().nextInt(0, UserAgents.userAgents.length);
      document = Jsoup.connect(URL).userAgent(UserAgents.userAgents[randomUserAgent]).cookies(mainCookies).get();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return document;
  }
}
