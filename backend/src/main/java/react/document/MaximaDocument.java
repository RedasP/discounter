package react.document;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MaximaDocument implements JsoupDocument {

  String HTML;
  private final Lock lock = new ReentrantLock();

  public MaximaDocument() {
  }

  @Override
  public Document getDocument(String URL, String itemName) {
    this.HTML = "";
    int n = 6;
    long startTime = System.currentTimeMillis();

    ArrayList<Thread> threads = new ArrayList<>();
    for(int i = 1; i <= n; i++){
      threads.add(new Thread(new ParallelRead(URL, itemName, i)));
    }

    for(int i = 0; i < n; i++){
      threads.get(i).start();
    }

    for(int i = 0; i < n; i++) {
      try {
        threads.get(i).join();

      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println("Reading took "+(System.currentTimeMillis()-startTime));
    return Jsoup.parse(HTML);
  }

  class ParallelRead implements Runnable {

    private String URL;
    private String itemName;
    private int pageNumber;

    public ParallelRead(String URL, String itemName, int pageNumber){
      this.pageNumber = pageNumber;
      this.URL = URL;
      this.itemName = itemName;
    }

    @Override
    public void run() {
      try {
        int randomUserAgent = ThreadLocalRandom.current().nextInt(0, UserAgents.userAgents.length);
        Document document = Jsoup.connect(URL + itemName + "&page=" + pageNumber).userAgent(UserAgents.userAgents[randomUserAgent]).maxBodySize(0).get();
        document.select("div.content-block.white-bg").remove();
        lock.lock();
        HTML += document.html();
        lock.unlock();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }
}
