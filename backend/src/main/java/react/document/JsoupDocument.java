package react.document;

import org.jsoup.nodes.Document;

public interface JsoupDocument {
  Document getDocument(String URL, String itemName);
}
