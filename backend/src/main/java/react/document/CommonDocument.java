package react.document;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class CommonDocument implements JsoupDocument {
  @Override
  public Document getDocument(String URL, String itemName) {
    Document document = null;
    try {
        int randomUserAgent = ThreadLocalRandom.current().nextInt(0, UserAgents.userAgents.length);
        document = Jsoup.connect(URL).userAgent(UserAgents.userAgents[randomUserAgent]).maxBodySize(0).get();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return document;
  }
}
