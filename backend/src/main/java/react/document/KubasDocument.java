package react.document;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class KubasDocument implements JsoupDocument {
  private String HTML;
  private final Lock lock = new ReentrantLock();
  private List<String> URLs = new ArrayList<>();

  public KubasDocument() {
  }

  @Override
  public Document getDocument(String URL, String itemName) {
    this.HTML = "";
    int n = 9;
    long startTime = System.currentTimeMillis();

    URLs.add("http://www.pckubas.lt/akcijos/bakaleja-5");
    URLs.add("http://www.pckubas.lt/akcijos/duonos-gaminiai-ir-konditerija-3");
    URLs.add("http://www.pckubas.lt/akcijos/gerimai-7");
    URLs.add("http://www.pckubas.lt/akcijos/kosmetika-ir-higiena-9");
    URLs.add("http://www.pckubas.lt/akcijos/kudikiu-ir-vaiku-prekes-8");
    URLs.add("http://www.pckubas.lt/akcijos/mesa-zuvys-ir-kulinarija-4");
    URLs.add("http://www.pckubas.lt/akcijos/namu-ukio-ir-gyvunu-prekes-1");
    URLs.add("http://www.pckubas.lt/akcijos/pieno-gaminiai-ir-kiausiniai-2");
    URLs.add("http://www.pckubas.lt/akcijos/saldytas-maistas-6");

    ArrayList<Thread> threads = new ArrayList<>();
    for(int i = 0; i < n; i++){
      threads.add(new Thread(new ParallelRead(i)));
    }

    for(int i = 0; i < n; i++){
      threads.get(i).start();
    }

    for(int i = 0; i < n; i++) {
      try {
        threads.get(i).join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println("Reading took "+(System.currentTimeMillis()-startTime));
    return Jsoup.parse(HTML);
  }

  class ParallelRead implements Runnable {
    private int pageNumber;

    public ParallelRead(int pageNumber){
      this.pageNumber = pageNumber;
    }

    @Override
    public void run() {
      try {
        int randomUserAgent = ThreadLocalRandom.current().nextInt(0, UserAgents.userAgents.length);
        Document document = Jsoup.connect(URLs.get(pageNumber)).userAgent(UserAgents.userAgents[randomUserAgent]).maxBodySize(0).get();
        lock.lock();
        HTML += document.html();
        lock.unlock();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }
}
