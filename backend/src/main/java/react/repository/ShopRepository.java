package react.repository;

import org.springframework.data.repository.CrudRepository;
import react.model.Shop;

public interface ShopRepository extends CrudRepository<Shop, Long> {
}
