package react.repository;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Repository;
import react.analyser.Analyser;
import react.document.JsoupDocument;
import react.model.ItemResponse;
import react.repository.ItemRepository;

import java.io.IOException;
import java.util.Map;

@Repository
public class JsoupItemRepository implements ItemRepository {

  private Analyser analyser;
  private String URL;
  private String shopName;
  private String logoURL;
  private JsoupDocument jsoupDocument;

  public JsoupItemRepository() {
  }

  public JsoupItemRepository(Analyser analyser, String URL, String shopName, String logoURL, JsoupDocument jsoupDocument) {
    this.analyser = analyser;
    this.URL = URL;
    this.shopName = shopName;
    this.logoURL = logoURL;
    this.jsoupDocument = jsoupDocument;
  }

  @Override
  public ItemResponse getItems(String itemName) {
    Document document = jsoupDocument.getDocument(URL, itemName);
    long startTime = System.currentTimeMillis();
    analyser.setDocument(document);
    ItemResponse itemResponse = new ItemResponse(shopName, logoURL, analyser.analyseDocument(itemName));
    System.out.println("Analysing took "+(System.currentTimeMillis()-startTime));
    return itemResponse;
  }

  public JsoupDocument getJsoupDocument() {
    return jsoupDocument;
  }

  public void setJsoupDocument(JsoupDocument jsoupDocument) {
    this.jsoupDocument = jsoupDocument;
  }

  public Analyser getAnalyser() {
    return analyser;
  }

  public void setAnalyser(Analyser analyser) {
    this.analyser = analyser;
  }

  public String getURL() {
    return URL;
  }

  public void setURL(String URL) {
    this.URL = URL;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getLogoURL() {
    return logoURL;
  }

  public void setLogoURL(String logoURL) {
    this.logoURL = logoURL;
  }

}
