package react.repository;

import org.springframework.data.repository.CrudRepository;
import react.model.Item;

public interface ItemNameRepository extends CrudRepository<Item, Long> {
}
