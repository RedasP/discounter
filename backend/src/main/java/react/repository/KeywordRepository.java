package react.repository;

import org.springframework.data.repository.CrudRepository;
import react.model.Keyword;

public interface KeywordRepository extends CrudRepository<Keyword, Long>{
}
