package react.repository;

import org.springframework.data.repository.CrudRepository;
import react.model.ItemDetails;

import java.util.List;

public interface DatabaseItemRepository extends CrudRepository<ItemDetails, Long> {
  List<ItemDetails> findByShopName(String shopName);
}
