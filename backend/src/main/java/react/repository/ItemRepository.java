package react.repository;

import react.model.ItemResponse;

public interface ItemRepository {
  ItemResponse getItems(String itemName);
}
