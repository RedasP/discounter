package react.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class ItemDetails {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long ID;
  @Column(nullable = true)
  private Double Price;
  private String Discount;
  private String Amount;
  private String ImageURL;
  private Date date;
  @OneToOne
  private Item item;
  @OneToOne
  private Shop shop;
  @ManyToMany
  private Collection<Keyword> keyword;

  public ItemDetails() {
  }

  public ItemDetails(Double price, String discount, String amount, String imageURL, Date date, Item item, Shop shop, Collection<Keyword> keyword) {
    Price = price;
    Discount = discount;
    Amount = amount;
    ImageURL = imageURL;
    this.date = date;
    this.item = item;
    this.shop = shop;
    this.keyword = keyword;
  }

  public Long getID() {
    return ID;
  }

  public void setID(Long ID) {
    this.ID = ID;
  }

  public Double getPrice() {
    return Price;
  }

  public void setPrice(Double price) {
    Price = price;
  }

  public String getDiscount() {
    return Discount;
  }

  public void setDiscount(String discount) {
    Discount = discount;
  }

  public String getAmount() {
    return Amount;
  }

  public void setAmount(String amount) {
    Amount = amount;
  }

  public String getImageURL() {
    return ImageURL;
  }

  public void setImageURL(String imageURL) {
    ImageURL = imageURL;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Item getItem() {
    return item;
  }

  public void setItem(Item item) {
    this.item = item;
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public Collection<Keyword> getKeyword() {
    return keyword;
  }

  public void setKeyword(Collection<Keyword> keyword) {
    this.keyword = keyword;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ItemDetails)) return false;

    ItemDetails itemDetails = (ItemDetails) o;

    return item.getName() != null ? item.getName().equals(itemDetails.item.getName()) : itemDetails.item.getName() == null;
  }

  @Override
  public int hashCode() {
    return item.getName() != null ? item.getName().hashCode() : 0;
  }
}
