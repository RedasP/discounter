package react.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import react.repository.*;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

  @Autowired
  @Qualifier("maximaRepository")
  private JsoupItemRepository maximaRepository;

  @Autowired
  @Qualifier("kubasRepository")
  private JsoupItemRepository kubasRepository;

  @Autowired
  @Qualifier("lidlRepository")
  private JsoupItemRepository lidlRepository;

  @Autowired
  @Qualifier("rimiRepository")
  private JsoupItemRepository rimiRepository;

  @Autowired
  private DatabaseItemRepository databaseItemRepository;

  @Autowired
  private ShopRepository shopRepository;

  @Autowired
  private ItemNameRepository itemNameRepository;

  @Autowired
  private KeywordRepository keywordRepository;

  public ItemResponse getMaximaItems(String itemName){
    return maximaRepository.getItems(itemName);
  }

  public ItemResponse getKubasItems(String itemName){
    return kubasRepository.getItems(itemName);
  }

  public ItemResponse getLidlItems(String itemName){
    return lidlRepository.getItems(itemName);
  }

  public ItemResponse getIkiItems(String itemName){
    List<ItemDetails> itemDetailsList = databaseItemRepository.findByShopName("Iki");
    itemDetailsList = filterItems(itemDetailsList, itemName);
    ItemResponse itemResponse = new ItemResponse("IKI","http://lofrev.net/wp-content/photos/2017/03/Iki_logo_2.png", itemDetailsList);
    return itemResponse;
  }

  public ItemResponse getRimiItems(String itemName){
    rimiRepository.setURL("https://app.rimi.lt/products/search/"+itemName);
    return rimiRepository.getItems(itemName);
  }

  public ItemResponse getNorfaItems(String itemName){
    List<ItemDetails> itemDetailsList = databaseItemRepository.findByShopName("Norfa");
    itemDetailsList = filterItems(itemDetailsList, itemName);
    ItemResponse itemResponse = new ItemResponse("Norfa","https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Norfa_Logo.svg/1280px-Norfa_Logo.svg.png", itemDetailsList);
    return itemResponse;
  }

  public ItemResponse getSilasItems(String itemName){
    List<ItemDetails> itemDetailsList = databaseItemRepository.findByShopName("Šilas");
    itemDetailsList = filterItems(itemDetailsList, itemName);
    ItemResponse itemResponse = new ItemResponse("Šilas","http://silas.lt/wp-content/uploads/2015/01/logo-be-sukio.png", itemDetailsList);
    return itemResponse;
  }

  public List<ItemDetails> filterItems(List<ItemDetails> itemDetailsList, String itemName){
    /*
    System.out.println("LAIKAS");
    System.out.println(new Date());
    System.out.println(itemDetailsList.get(0).getDate());
    System.out.println(new Date().compareTo(itemDetailsList.get(0).getDate()));
    */
    return itemDetailsList.stream().filter(item ->
      new Date().compareTo(item.getDate()) <= 0 &&
      ((item.getItem().getName().toLowerCase().contains(itemName.toLowerCase()))
        || item.getKeyword().stream()
        .anyMatch(keyword -> keyword.getKeyword().toLowerCase().contains(itemName.toLowerCase()))))
      .collect(Collectors.toList());
  }

  public <T> T shouldAdd(Iterable<T> elements, T newElement){
    for(T element : elements){
      if(element.equals(newElement))
        return element;
    }
    return null;
  }

  public void populate(){
    try {
      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("data.txt")), "UTF8"))) {
        String line;

        int shopCount = Integer.parseInt(br.readLine());

        for(int i = 0; i < shopCount; i++){
          String shopName = br.readLine();
          int productCount = Integer.parseInt(br.readLine());

          Shop shop = new Shop(shopName);

          Shop shop1 = shouldAdd(shopRepository.findAll(), shop);

          if(shop1 == null)
            shopRepository.save(shop);
          else
            shop = shop1;

          for(int x = 0; x < productCount; x++){
            line = br.readLine();
            String[] token = line.split("#");

            Item item = new Item(token[4]);

            Item item1 = shouldAdd(itemNameRepository.findAll(), item);
            if(item1 == null)
              itemNameRepository.save(item);
            else
              item = item1;

            List<Keyword> keywordList = new ArrayList<>();
            for(int z = 5; z < token.length; z++){
              Keyword keyword = new Keyword(token[z]);
              Keyword keyword1 = shouldAdd(keywordRepository.findAll(), keyword);
              if(keyword1 == null)
                keywordRepository.save(keyword);
              else
                keyword = keyword1;
              keywordList.add(keyword);
            }

            ItemDetails itemDetails = new ItemDetails();
            itemDetails.setItem(item);
            itemDetails.setShop(shop);
            itemDetails.setKeyword(keywordList);
            itemDetails.setImageURL("https://rimibaltic-res.cloudinary.com/image/upload/w_180,h_220,f_auto,q_auto,fl_lossy,c_pad,d_mobile-app:blank.png/MAT_182461_PCE_LT");

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, Integer.parseInt(token[0]));
            cal.set(Calendar.MONTH, Integer.parseInt(token[1]));
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(token[2]));
            itemDetails.setDate(cal.getTime());

            if(token[3].contains("%"))
              itemDetails.setDiscount(token[3]);
            else
              itemDetails.setPrice(Double.parseDouble(token[3]));

            if(shouldAdd(databaseItemRepository.findAll(), itemDetails) == null)
              databaseItemRepository.save(itemDetails);
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
