package react.model;

import java.util.List;

public class ItemResponse {

  private String name;
  private String logoURL;
  private List<ItemDetails> itemDetails;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<ItemDetails> getItemDetails() {
    return itemDetails;
  }

  public void setItemDetails(List<ItemDetails> itemDetails) {
    this.itemDetails = itemDetails;
  }

  public String getLogoURL() {
    return logoURL;
  }

  public void setLogoURL(String logoURL) {
    this.logoURL = logoURL;
  }

  public ItemResponse(String name, String logoURL, List<ItemDetails> itemDetails) {
    this.name = name;
    this.logoURL = logoURL;
    this.itemDetails = itemDetails;
  }

}
