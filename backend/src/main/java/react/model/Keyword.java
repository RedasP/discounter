package react.model;

import javax.persistence.*;

@Entity
public class Keyword {
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Long id;
  @Column(unique=true)
  private String keyword;

  public Keyword(){}

  public Keyword(String keyword){
    this.keyword = keyword;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Keyword keyword1 = (Keyword) o;

    return keyword != null ? keyword.equals(keyword1.keyword) : keyword1.keyword == null;
  }

  @Override
  public int hashCode() {
    return keyword != null ? keyword.hashCode() : 0;
  }
}
