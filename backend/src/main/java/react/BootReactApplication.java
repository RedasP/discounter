package react;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.session.SessionAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(exclude = SessionAutoConfiguration.class)
@ImportResource("classpath:/beans.xml")
public class BootReactApplication {

  public static void main(String[] args) {
    SpringApplication.run(BootReactApplication.class, args);
  }
}
