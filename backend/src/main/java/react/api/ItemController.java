package react.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import react.model.ItemResponse;
import react.model.ItemService;

@RestController
public class ItemController {

  @Autowired
  private ItemService itemService;

  @RequestMapping("/api/Maxima/{itemName}")
  public ItemResponse getMaximaItems(@PathVariable("itemName") String itemName){
    return itemService.getMaximaItems(itemName);
  }

  @RequestMapping("/api/Kubas/{itemName}")
  public ItemResponse getKubasItems(@PathVariable("itemName") String itemName){
    return itemService.getKubasItems(itemName);
  }

  @RequestMapping("/api/Lidl/{itemName}")
  public ItemResponse getLidlItems(@PathVariable("itemName") String itemName){
    return itemService.getLidlItems(itemName);
  }

  @RequestMapping("/api/Iki/{itemName}")
  public ItemResponse getIkiItems(@PathVariable("itemName") String itemName){
    return itemService.getIkiItems(itemName);
  }

  @RequestMapping("/api/Rimi/{itemName}")
  public ItemResponse getRimiItems(@PathVariable("itemName") String itemName){
    return itemService.getRimiItems(itemName);
  }

  @RequestMapping("/api/Norfa/{itemName}")
  public ItemResponse getNorfaItems(@PathVariable("itemName") String itemName){
    return itemService.getNorfaItems(itemName);
  }

  @RequestMapping("/api/Silas/{itemName}")
  public ItemResponse getSilasItems(@PathVariable("itemName") String itemName){
    return itemService.getSilasItems(itemName);
  }

  @RequestMapping("/api/populate")
  public void populate(){
    itemService.populate();
  }


}
