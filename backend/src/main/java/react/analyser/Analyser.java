package react.analyser;

import org.jsoup.nodes.Document;
import react.model.ItemDetails;

import java.util.List;

public interface Analyser {

  void setDocument(Document document);

  List<ItemDetails> analyseDocument(String searchFor);

}
