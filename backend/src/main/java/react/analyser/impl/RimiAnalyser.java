package react.analyser.impl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import react.analyser.Analyser;
import react.model.Item;
import react.model.ItemDetails;
import react.model.Shop;

import java.util.ArrayList;
import java.util.List;

public class RimiAnalyser implements Analyser {

  private Document document;

  public Document getDocument() {
    return document;
  }

  @Override
  public void setDocument(Document document) {
    this.document = document;
  }

  public RimiAnalyser(){}

  @Override
  public List<ItemDetails> analyseDocument(String searchFor) {
    ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();

    Elements scriptElements = document.getElementsByTag("script");

    for (Element element :scriptElements ){
      for (DataNode node : element.dataNodes()) {
        String wholeData = node.getWholeData();
        String lines[] = wholeData.split("\n");
        for(String line : lines)
        {
          if(line.contains("page.init")){
            String pageInit = line.substring(26).replace(");","");
            String root = pageInit.substring(0, pageInit.indexOf(", {"));
            System.out.println(root);
            try {
              JSONObject json = new JSONObject(root);
              JSONArray categories = json.getJSONArray("categories");
              for(int i = 0; i < categories.length(); i++){
                JSONArray entries = categories.getJSONObject(i).getJSONArray("entries");
                for(int x = 0; x < entries.length(); x++){
                  System.out.println(entries.getJSONObject(x).get("name"));


                  ItemDetails itemDetails = new ItemDetails();

                  Item item = new Item(entries.getJSONObject(x).get("name").toString());
                  Shop shop = new Shop("Rimi");

                  itemDetails.setItem(item);
                  itemDetails.setShop(shop);
                  itemDetails.setPrice(Double.parseDouble(entries.getJSONObject(x).get("price").toString()));

                  JSONArray imageURLArray = entries.getJSONObject(x).getJSONArray("product_images");
                  String imageURL = "https://rimibaltic-res.cloudinary.com/image/upload/w_180,h_220,f_auto,q_auto,fl_lossy,c_pad,d_mobile-app:blank.png/" + imageURLArray.getJSONObject(0).getString("template_image_id");
                  itemDetails.setImageURL(imageURL);
                  if(!itemDetailsList.contains(itemDetails))
                    itemDetailsList.add(itemDetails);
                }
              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

        }
      }
    }
    return itemDetailsList;
  }
}
