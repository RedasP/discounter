package react.analyser.impl;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import react.analyser.Analyser;
import react.model.Item;
import react.model.ItemDetails;
import react.model.Shop;

import java.util.ArrayList;

public class LidlAnalyser implements Analyser {

  private Document document;

  public Document getDocument() {
    return document;
  }

  @Override
  public void setDocument(Document document) {
    this.document = (Document) document;
  }

  @Override
  public ArrayList<ItemDetails> analyseDocument(String searchFor) {
    ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();

    Elements elements = document.getElementsByClass("productgrid__item");
    for (Element element : elements) {

      String productTitle = element.getElementsByClass("product__title").first().text();
      if (productTitle.toLowerCase().contains(searchFor.toLowerCase())) {
        ItemDetails itemDetails = new ItemDetails();
        Item item = new Item(productTitle);
        Shop shop = new Shop("Lidl");

        String value = element.getElementsByClass("pricefield__price").first().ownText();
        String cents = element.getElementsByClass("pricefield__price-superscript").first().ownText();;
        itemDetails.setPrice(Double.parseDouble(value+"."+cents));

        itemDetails.setItem(item);
        itemDetails.setShop(shop);
        itemDetails.setAmount(element.getElementsByClass("pricefield__footer").first().text());

        String imageURL = element.select("div.product__image img").attr("data-srcset");
        String[] splitURL = imageURL.split(",");
        String[] splitLine = splitURL[splitURL.length-1].split(" ");
        if(splitLine.length > 1)
          itemDetails.setImageURL(splitLine[1]);

        itemDetailsList.add(itemDetails);
      }
    }

    return itemDetailsList;
  }
}
