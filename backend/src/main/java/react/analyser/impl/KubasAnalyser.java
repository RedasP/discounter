package react.analyser.impl;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import react.analyser.Analyser;
import react.model.Item;
import react.model.ItemDetails;
import react.model.Shop;

import java.util.ArrayList;

public class KubasAnalyser implements Analyser {

  private Document document;

  public Document getDocument() {
    return document;
  }

  @Override
  public void setDocument(Document document) {
    this.document = (Document) document;
  }

  @Override
  public ArrayList<ItemDetails> analyseDocument(String searchFor) {
    ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();

    Elements elements = document.select("div.itemBox.col-lg-12.noPadd");

    for (Element element : elements) {
      String itemName = element.select("div.itemName").text();
      if(itemName.toLowerCase().contains(searchFor.toLowerCase())){
        ItemDetails itemDetails = new ItemDetails();
        Item item = new Item(itemName);
        Shop shop = new Shop("Kubas");

        String value = element.select("div.price b").first().ownText().replace("€", "");
        String cents = element.select("div.price b sup").first().ownText();
        itemDetails.setPrice(Double.parseDouble(value+"."+cents));
        itemDetails.setItem(item);
        itemDetails.setShop(shop);
        itemDetails.setImageURL(element.select("img.img-responsive.center-block").attr("src"));

        itemDetailsList.add(itemDetails);
      }

    }

    return itemDetailsList;
  }
}
