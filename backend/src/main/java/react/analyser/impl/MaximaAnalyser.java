package react.analyser.impl;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import react.analyser.Analyser;
import react.model.Item;
import react.model.ItemDetails;
import react.model.Shop;

import java.util.ArrayList;

@Component
public class MaximaAnalyser implements Analyser {

  private Document document;

  public MaximaAnalyser() {
  }

  @Override
  public void setDocument(Document document) {
    this.document = (Document) document;
  }

  public Document getDocument() {
    return document;
  }

  @Override
  public ArrayList analyseDocument(String searchFor) {
    ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();

    Elements elements = document.select("div.b-product--wrap.clearfix.b-product--js-hook");

    for (Element element : elements) {
      ItemDetails itemDetails = new ItemDetails();

      String object = element.attr("data-b-for-cart");
      try {
        JSONObject json = new JSONObject(object);

        Item item = new Item(json.get("title").toString());
        Shop shop = new Shop("Maxima");

        itemDetails.setItem(item);
        itemDetails.setShop(shop);
        itemDetails.setPrice(Double.parseDouble(json.get("price").toString()));
        itemDetails.setImageURL("https://www.barbora.lt"+json.get("image").toString());
      } catch (JSONException e) {
        e.printStackTrace();
      }
      itemDetailsList.add(itemDetails);
    }

    return itemDetailsList;
  }

}
